<?php

class CompanyRequest extends Database {

    private $id;
    private $companyId;

    private $type;
    private $desc;


    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "request":
                $this->type = $args["req_type"];
                $this->desc = $args["desc"];
                $this->companyId = $args["companyId"];
            break;
            case "edit_request":
                $this->type = $args["req_type"];
                $this->desc = $args["desc"];
                $this->id = $args["id"];
            break;
            case "fetch_request":
                $this->companyId = $args["companyId"];
            break;
            default:

            break;
        }
    }


    public function createRequest() {
        
        $this->createConn();

        $this->query("INSERT INTO 
                    company_request (request_type, request_desc, company_id)
                    VALUES
                    ('". $this->type ."', '". $this->desc ."', '". $this->companyId ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;
    }


    public function updateRequest() {

        $this->createConn();

        $this->query("UPDATE company_request 
                    SET 
                    request_type = '". $this->type ."', request_desc = '". $this->desc ."'
                    WHERE request_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM company_request WHERE company_id = '". $this->companyId ."' ORDER BY request_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

}