<?php

class CompanyRequirement extends Database {

    private $id;
    private $companyId;

    private $reqName;


    private $res;

    public function __construct($args, $req_code) {

        switch ($req_code) {
            case "requirement":
                $this->reqName = $args["req_name"];
                $this->companyId = $args["companyId"];
            break;
            case "delete_requirement":
                $this->reqName = $args["req_name"];
                $this->id = $args["id"];
            break;
            case "fetch_requirement":
                $this->companyId = $args["companyId"];
            break;
            default:

            break;
        }
    }


    public function createRequirement() {
        
        $this->createConn();

        $this->query("INSERT INTO 
                    company_req (comp_req_desc, company_id)
                    VALUES
                    ('". $this->reqName ."', '". $this->companyId ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;
    }


    public function deleteRequirement() {

        $this->createConn();

        $this->query("UPDATE company_req 
                    SET 
                    comp_req_desc = '". $this->reqName ."'
                    WHERE comp_req_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM company_req WHERE company_id = '". $this->companyId ."' ORDER BY comp_req_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

}