<?php

require('../config.php');
require('Database.php');
require('Validation.php');
require('Auth.php');
require('JobPosting.php');
require('CompanyRequest.php');
require('CompanyRequirement.php');


$json_response = array();
$validation = new Validation();

if (isset($_POST["request"])) {
    $req = $_POST["request"];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    if ($validation->checkItems($_POST)) {
        
        switch ($req) {
            case "login":
                $auth = new Auth($_POST, "login");
                $json_response = $auth->login();
            break;
            case "forgot_pass":
                $auth = new Auth($_POST, "forgot_pass");
                $json_response = $auth->forgotPass();
            break;

            
            case "job_post":
                $jobPosting = new JobPosting($_POST, "job_post");
                $json_response = $jobPosting->createJobPost();
                // print_r($_POST);
            break;
            case "edit_job_post":
                $jobPosting = new JobPosting($_POST, "edit_job_post");
                $json_response = $jobPosting->updateJobPost();
            break;            
            case "fetch_job_post":
                $jobPosting = new JobPosting($_POST, "fetch_job_post");
                $json_response = $jobPosting->fetchAll();            
            break;


            case "request":
                $companyRequest = new CompanyRequest($_POST, "request");
                $json_response = $companyRequest->createRequest();
                // print_r($_POST);
            break;
            case "edit_request":
                $companyRequest = new CompanyRequest($_POST, "edit_request");
                $json_response = $companyRequest->updateRequest();
            break;            
            case "fetch_request":
                $companyRequest = new CompanyRequest($_POST, "fetch_request");
                $json_response = $companyRequest->fetchAll();            
            break;


            case "requirement":
                $companyRequirement = new CompanyRequirement($_POST, "requirement");
                $json_response = $companyRequirement->createRequirement();
                // print_r($_POST);
            break;
            case "delete_requirement":
                $companyRequirement = new CompanyRequirement($_POST, "delete_requirement");
                $json_response = $companyRequirement->deleteRequirement();
            break;            
            case "fetch_requirement":
                $companyRequirement = new CompanyRequirement($_POST, "fetch_requirement");
                $json_response = $companyRequirement->fetchAll();            
            break;



            default:
                $json_response["success"] = false;
                $json_response["result"] = "Unknown Request";
            break;
        }
        
    } else {
        $json_response["success"] = false;
        $json_response["result"] = "Empty Post Value";        
    }

} else {
    $json_response["success"] = false;
    $json_response["result"] = "Empty Request Value";
}

echo json_encode($json_response);