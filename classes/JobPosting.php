<?php

class JobPosting extends Database {

    private $id;
    private $companyId;

    private $title;
    private $educAttain;
    private $expertise;
    private $salary;
    private $vacancy;
    private $jobCat;
    private $desc;


    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "job_post":
                $this->title = $args["title"];
                $this->educAttain = $args["educAttain"];
                $this->expertise = $args["expertise"];
                $this->salary = $args["salary"];
                $this->vacancy = $args["vacancy"];
                $this->jobCat = $args["jobCat"];
                $this->desc = $args["desc"];
                $this->companyId = $args["companyId"];
            break;
            case "edit_job_post":
                $this->title = $args["title"];
                $this->educAttain = $args["educAttain"];
                $this->expertise = $args["expertise"];
                $this->salary = $args["salary"];
                $this->vacancy = $args["vacancy"];
                $this->jobCat = $args["jobCat"];
                $this->desc = $args["desc"];
                $this->id = $args["id"];
            break;
            case "fetch_job_post":
                $this->companyId = $args["companyId"];
            break;
            default:

            break;
        }
    }


    public function createJobPost() {
        
        $this->createConn();

        $this->query("INSERT INTO 
                    company_posting (post_title, post_desc, post_educ, post_exp, post_salary, post_cat, post_vacancy, company_id)
                    VALUES
                    ('". $this->title ."', '". $this->desc ."', '". $this->educAttain ."', '". $this->expertise ."',
                    '". $this->salary ."', '". $this->jobCat ."', '". $this->vacancy ."', '". $this->companyId ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;
    }


    public function updateJobPost() {

        $this->createConn();

        $this->query("UPDATE company_posting 
                    SET 
                    post_title = '". $this->title ."', post_desc = '". $this->desc ."', 
                    post_educ = '". $this->educAttain ."', post_exp = '". $this->expertise ."', 
                    post_salary = '". $this->salary ."', post_cat = '". $this->jobCat ."', 
                    post_vacancy = '". $this->vacancy ."', post_status = 'Pending'
                    
                    WHERE post_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM company_posting WHERE company_id = '". $this->companyId ."' ORDER BY post_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

}