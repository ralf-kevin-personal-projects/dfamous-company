<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Profile</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <!-- <button class="btn btn-md btn-outline-secondary">Add New Applicant</button> -->
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>

        
        <div class="form-group">

            <h6>Company Information</h6>
            <div class="row">
                    <div class="col-md-12">
                        <label>Image</label>                                                                
                        <div class="form-group">
                            <input type="file" class="form-control"/>
                        </div>
                    </div>                
                    <div class="col-md-12">
                        <label>Company Name</label>                                                                
                        <div class="form-group">
                            <input id="name" type="text" class="form-control" placeholder="Company Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Address</label>                                                                
                        <div class="form-group">
                            <input id="address" type="text" class="form-control" placeholder="Address"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Location</label>                                                                
                        <div class="form-group">
                            <input id="location" type="text" class="form-control" placeholder="Location"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Description</label>                                                                
                        <div class="form-group">
                            <textarea id="desc" class="form-control" placeholder="Description" rows=5></textarea>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                        <label>Email</label>                                                                
                        <div class="form-group">
                            <input id="email" type="email" class="form-control" placeholder="Email"/>
                        </div>
                    </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input id="number" type="number" class="form-control" placeholder="Contact Number"/>
                    </div>                       
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button id="btn-save" class="btn btn-md btn-success">Save Changes</button>
                </div>
            </div> 
        
        </div>


        </main>

<?php include '../headers/dashboard-footer.php'; ?>


<script>
    $(document).ready(function() {

        loadSessions();

        function loadSessions() {
            if (typeof(Storage) !== "undefined") {

                if (localStorage.length > 0) {

                    var id = localStorage.getItem("company_id");
                    var name = localStorage.getItem("company_name");
                    var add = localStorage.getItem("company_address");
                    var loc = localStorage.getItem("company_location");
                    var desc = localStorage.getItem("company_desc");
                    var email = localStorage.getItem("company_email");
                    var contact = localStorage.getItem("company_contact");
                    
                    $("#name").val(name);
                    $("#address").val(add);
                    $("#location").val(loc);
                    $("#desc").val(desc);
                    $("#email").val(email);
                    $("#number").val(contact);

                } else {
                    location.href = "../index.php";                    
                }


            } else {

                alert("Your Browser is not supported to our function, please download better browser")
                location.href = "../index.php";

            }
        }

    });


</script>