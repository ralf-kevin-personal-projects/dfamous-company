<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Request</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#requestModal">Create Request</button>
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>
        
        <div class="">

                <div class="table-responsive">
                    <table id="tbl-requests" class="table table-striped table-sm">
                        <thead>
                            <tr>
                            <th style="width:15%;">Type</th>
                            <th style="width:40%;">Description</th>
                            <th>Date Created</th>
                            <th>Status</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                    <tbody>                           
                    </tbody>
                    </table>
                </div>
        </div>


        </main>



<!-- Modal -->
<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="requestModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="requestModalTitle">Create Request</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">
            
                <!-- FOR REQUEST ID -->
                <input id="id" type="hidden"/>

                <h6>New Request</h6>
                <div class="row">
                        <div class="col-md-12 selectApplicant">
                            <label>Request Type</label>                                                                
                            <div class="form-group">
                                <select id="req-type" class="form-control">
                                    <option value="">Select Request</option>
                                    <option value="Additional Employee">Additional Employee</option>
                                    <option value="Exchange">Exchange</option>
                                    <option value="Terminate">Terminate</option>
                                </select>
                            </div>
                        </div>                
                        <div class="col-md-12">
                            <label>Description</label>                                                                
                            <div class="form-group">
                                <textarea id="desc" class="form-control" placeholder="Description" rows=10></textarea>
                            </div>
                        </div>
                </div>



            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button type="button" id="btnSave" class="btn btn-success">Create Request</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){

        var company_id = localStorage.getItem("company_id");

        loadData();
        
        $(document).on("click", "#btn-edit", function(){
            var id = $(this).data("id");
            var req_type = $(this).data("type");
            var desc = $(this).data("desc");

            $("#id").val(id);
            $("#req-type").val(req_type).attr("disabled", false);
            $("#desc").val(desc).attr("disabled", false);


            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#requestModalTitle").text("Edit Job Post");            
            $("#requestModal").modal("show");
        });

        $(document).on("click", "#btn-view", function(){

            var id = $(this).data("id");
            var req_type = $(this).data("type");
            var desc = $(this).data("desc");

            $("#id").val(id);
            $("#req-type").val(req_type).attr("disabled", true);
            $("#desc").val(desc).attr("disabled", true);

            $("#btnSave").hide();
            $("#requestModalTitle").text("View Job Post");            
            $("#requestModal").modal("show");

        });


        $(document).on("click", "#btn-delete", function(){
            alert("Calling delete function")
        });


        $("#btnSave").click(function(){

            var cmdType = $(this).text();

            var req_type_key = "#req-type";
            var desc_key = "#desc";

            var id = $("#id").val();

            var req_type = $(req_type_key).val();
            var desc = $(desc_key).val();
            

            var values = [req_type, desc];
            var keys = [req_type_key, desc_key];


            if (validateItems(values, keys)) {

                switch(cmdType) {
                    case "Create Request":
                        companyRequests(values, company_id, "create");
                    break;
                    case "Save Changes":
                        companyRequests(values, id, "update");
                    break;
                    default:
                        alert("Unknown Function")
                    break;
                }


            } else {
                alert("please fill up the empty items");
            }

        });





        function companyRequests(params, id, type) {

            var fd = new FormData();

            switch(type) {
                case "create":
                    fd.append("companyId", id);
                    fd.append("request", "request");
                break;
                case "update":
                    fd.append("id", id);
                    fd.append("request", "edit_request");
                break;
            }

            fd.append("req_type", params[0]);
            fd.append("desc", params[1]);

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    alert("response: " + res.result)
                    console.log(res);
                    location.reload();
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function loadData() {

            var fd = new FormData();
            fd.append("companyId", company_id);
            fd.append("request", "fetch_request");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl = "";

            if (datas.length > 0) {
                for (var i = 0; i < datas.length; i++) {

                    var id = datas[i]["request_id"];
                    var type = datas[i]["request_type"];
                    var desc = datas[i]["request_desc"];
                    var status = datas[i]["status"];
                    var date = datas[i]["date_created"];
                    
                    tmpl += "<tr>"+
                                "<td>"+ type +"</td>"+
                                "<td>"+ desc +"</td>"+
                                "<td>"+ date +"</td>"+
                                "<td>"+ status +"</td>"+
                                "<td>"+
                                    "<div class='form-group'>"+
                                        "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                            "data-type='"+ type +"' "+
                                            "data-desc='"+ desc +"' "+
                                            "data-id='"+ id +"' >"+
                                            "<i class='fas fa-edit'></i>"+
                                        "</button> "+
                                        "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                            "data-type='"+ type +"' "+
                                            "data-desc='"+ desc +"' "+
                                            "data-id='"+ id +"' >"+
                                            "<i class='fas fa-eye'></i>"+
                                        "</button> "+
                                        "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                            "data-id='"+ id +"' >"+
                                            "<i class='fas fa-trash'></i>"+
                                        "</button> "+
                                "</div>"+
                                "</td>"+
                            "</tr>";
                }
            } else {
                Alert("No Result");
            }

            $("#tbl-requests").find("tbody tr").remove().end();
            $("#tbl-requests").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }


    });
</script>