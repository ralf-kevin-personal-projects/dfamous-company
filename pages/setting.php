<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Password Setting</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <!-- <button class="btn btn-md btn-outline-secondary">Add New Applicant</button> -->
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>

        
        <div class="form-group">

            <div class="col-md-5 card card-body">
                <div class="col-md-12">
                    <h6>Password Setting</h6>
                    <div class="form-group">       
                        <label>Password</label>                                                                  
                        <input type="password" class="form-control" placeholder="Password"/>
                    </div>                       
                </div>
                <div class="col-md-12">
                    <div class="form-group">       
                        <label>Confirm Password</label>                                                                  
                        <input type="password" class="form-control" placeholder="Confirm Password"/>
                    </div>                       
                </div>
                <div class="col-md-12">
                    <button class="btn btn-md btn-success">Save Changes</button>
                </div>

            </div>
        </div>


        </main>

<?php include '../headers/dashboard-footer.php'; ?>

<script>
    function getAction() {
        var value = document.getElementById("status");
    }    

</script>