<?php include '../headers/dashboard-header.php'; ?>
            
      
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Job Posting</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
            <button class="btn btn-sm btn-outline-secondary">Export</button> -->
            <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#jpModal">Add New Job Post</button>
            
        </div>
        <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
            This week
        </button> -->
        </div>
    </div>
    
    <div class="">

            <div class="table-responsive">
                <table id="tbl-jobs" class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>Title</th>
                    <th>Expertise</th>
                    <th>Date Created</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>              
                </tbody>
                </table>
            </div>
    </div>


    </main>



<!-- Modal -->
<div class="modal fade" id="jpModal" tabindex="-1" role="dialog" aria-labelledby="jpModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="jpModalTitle">Add New Job Post</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">

                <!-- FOR POST ID -->
                <input id="id" type="hidden"/>

                <h6>Job Post</h6>
                <div class="row">
                        <div class="col-md-12 selectApplicant">
                            <label>Title</label>                                                                
                            <div class="form-group">
                                <input id="title" type="text" class="form-control " placeholder="Title"/>
                            </div>
                        </div>                
                        <div class="col-md-12">
                            <label>Educational Attainment</label>                                                                
                            <div class="form-group">
                                <input id="educ_attain" type="text" class="form-control" placeholder="Educational Attainment"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Expertise</label>                                                                
                            <div class="form-group">
                                <input id="expertise" type="text" class="form-control" placeholder="Expertise"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Salary</label>                                                                
                        <div class="form-group">
                            <input id="salary" type="text" class="form-control" placeholder="Salary"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">       
                            <label>Number of Vacancy</label>                                                                  
                            <input id="vacancy" type="number" class="form-control" placeholder="Number of Vacancy"/>
                        </div>                       
                    </div>
                </div>
                <hr/>

                <div class="row">
                        <div class="col-md-12">
                            <label>Job Category</label>                                                                
                            <div class="form-group">
                                <input id="job_cat" type="text" class="form-control" placeholder="Job Category"/>
                            </div>
                        </div>                
                        <div class="col-md-12">
                            <label>Description</label>                                                                
                            <div class="form-group">
                                <textarea id="description" class="form-control" placeholder="Description" rows=10></textarea>
                            </div>
                        </div>
                </div>

            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button type="button" id="btnSave" class="btn btn-success">Create Job Post</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){

        var company_id = localStorage.getItem("company_id");

        loadData();
        
        $(document).on("click", "#btn-edit", function(){
            var id = $(this).data("id");
            var title = $(this).data("title");
            var educ = $(this).data("educ");
            var exp = $(this).data("exp");
            var salary = $(this).data("salary");
            var vacancy = $(this).data("vacancy");
            var jcat = $(this).data("jcat");
            var desc = $(this).data("desc");

            $("#id").val(id);
            $("#title").val(title).attr("disabled", false);
            $("#educ_attain").val(educ).attr("disabled", false);
            $("#expertise").val(exp).attr("disabled", false);
            $("#salary").val(salary).attr("disabled", false);
            $("#vacancy").val(vacancy).attr("disabled", false);
            $("#job_cat").val(jcat).attr("disabled", false);
            $("#description").val(desc).attr("disabled", false);


            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#jpModalTitle").text("Update Job Post");
            $("#jpModal").modal("show");
        });

        $(document).on("click", "#btn-view", function(){

            var id = $(this).data("id");
            var title = $(this).data("title");
            var educ = $(this).data("educ");
            var exp = $(this).data("exp");
            var salary = $(this).data("salary");
            var vacancy = $(this).data("vacancy");
            var jcat = $(this).data("jcat");
            var desc = $(this).data("desc");

            $("#title").val(title).attr("disabled", true);
            $("#educ_attain").val(educ).attr("disabled", true);
            $("#expertise").val(exp).attr("disabled", true);
            $("#salary").val(salary).attr("disabled", true);
            $("#vacancy").val(vacancy).attr("disabled", true);
            $("#job_cat").val(jcat).attr("disabled", true);
            $("#description").val(desc).attr("disabled", true);

            $("#btnSave").hide();
            $("#jpModalTitle").text("View Job Post");
            $("#jpModal").modal("show");

        });


        $(document).on("click", "#btn-delete", function(){
            alert("Calling delete function")
        });


        $("#btnSave").click(function(){

            var cmdType = $(this).text();

            var title_key = "#title";
            var educAttain_key = "#educ_attain";
            var expertise_key = "#expertise";
            var salary_key = "#salary";
            var vacancy_key = "#vacancy";
            var jobCat_key = "#job_cat";
            var desc_key = "#description";

            var id = $("#id").val();

            var title = $(title_key).val();
            var educAttain = $(educAttain_key).val();
            var expertise = $(expertise_key).val();
            var salary = $(salary_key).val();
            var vacancy = $(vacancy_key).val();
            var jobCat = $(jobCat_key).val();
            var desc = $(desc_key).val();

            var values = [title, educAttain, expertise, salary, vacancy, jobCat, desc];
            var keys = [title_key, educAttain_key, expertise_key, salary_key, vacancy_key, jobCat_key, desc_key];


            if (validateItems(values, keys)) {

                switch(cmdType) {
                    case "Create Job Post":
                        jobPostRequests(values, company_id, "create");
                    break;
                    case "Save Changes":
                        jobPostRequests(values, id, "update");
                    break;
                    default:
                        alert("Unknown Function")
                    break;
                }


            } else {
                alert("please fill up the empty items");
            }

        });






        function jobPostRequests(params, id, type) {

            var fd = new FormData();

            switch(type) {
                case "create":
                    fd.append("companyId", id);
                    fd.append("request", "job_post");
                break;
                case "update":
                    fd.append("id", id);
                    fd.append("request", "edit_job_post");
                break;
            }

            fd.append("title", params[0]);
            fd.append("educAttain", params[1]);
            fd.append("expertise", params[2]);
            fd.append("salary", params[3]);
            fd.append("vacancy", params[4]);
            fd.append("jobCat", params[5]);
            fd.append("desc", params[6]);

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    alert("response: " + res.result)
                    console.log(res);
                    location.reload();
                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function loadData() {

            var fd = new FormData();
            fd.append("companyId", company_id);
            fd.append("request", "fetch_job_post");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    // alert("response " + res.result[2]["post_title"])
                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }


                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl = "";

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var id = datas[i]["post_id"];
                    var title = datas[i]["post_title"];
                    var educ = datas[i]["post_educ"];
                    var exp = datas[i]["post_exp"];
                    var salary = datas[i]["post_salary"];
                    var vacancy = datas[i]["post_vacancy"];
                    var jcat = datas[i]["post_cat"];
                    var desc = datas[i]["post_desc"];
                    var status = datas[i]["post_status"];
                    var date = datas[i]["publish_date"];

                    tmpl += "<tr>"+
                                "<td>"+ title +"</td>"+
                                "<td>"+ exp +"</td>"+
                                "<td>"+ date +"</td>"+
                                "<td>"+ status +"</td>"+
                                "<td>"+
                                    "<div class='form-group'>"+
                                        "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                            "data-title='"+ title +"' "+
                                            "data-educ='"+ educ +"' "+
                                            "data-exp='"+ exp +"' "+
                                            "data-salary='"+ salary +"' "+
                                            "data-vacancy='"+ vacancy +"' "+
                                            "data-jcat='"+ jcat +"' "+
                                            "data-desc='"+ desc +"' "+
                                            "data-id='"+ id +"' >"+
                                            "<i class='fas fa-edit'></i>"+
                                        "</button> "+
                                        "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                            "data-title='"+ title +"' "+
                                            "data-educ="+ educ +"' "+
                                            "data-exp="+ exp +"' "+
                                            "data-salary='"+ salary +"' "+
                                            "data-vacancy='"+ vacancy +"' "+
                                            "data-jcat='"+ jcat +"' "+
                                            "data-desc='"+ desc +"' "+
                                            "data-id='"+ id +"' >"+
                                            "<i class='fas fa-eye'></i>"+
                                        "</button> "+                                
                                        "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                            "data-id='"+ id +"' >"+
                                            "<i class='fas fa-trash'></i>"+
                                        "</button> "+
                                    "</div>"+
                                "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-jobs").find("tbody tr").remove().end();
            $("#tbl-jobs").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }

    });
</script>