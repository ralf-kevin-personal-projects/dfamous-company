<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Requirements</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#reqModal" >Add New Requirements</button>
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>
        
        <div class="">

                <div class="table-responsive">
                    <h5>Company Requirements</h5>
                    <table id="tbl-requirements" class="table table-striped table-sm">
                    <thead>
                        <tr>
                        <th>Requirement(s)</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
        </div>

        </main>



<!-- Modal -->
<div class="modal fade" id="reqModal" tabindex="-1" role="dialog" aria-labelledby="reqModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reqModalTitle">Add New Requirement</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">
            
                <!-- FOR REQUIREMENT ID -->
                <input id="id" type="hidden"/>

                <h6>Requirement</h6>
                <div class="row">              
                    <div class="col-md-12">
                        <label>Requirement Name</label>                                                                
                        <div class="form-group">
                            <input id="req-name" type="text" class="form-control" placeholder="Requirement Name"/>
                        </div>
                    </div>
                </div>
            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button type="button" id="btnSave" class="btn btn-success">Create Requirement</button>
      </div>
    </div>
  </div>
</div>

<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){

        var company_id = localStorage.getItem("company_id");

        loadData();
        
        $(document).on("click", "#btn-edit", function(){
            var id = $(this).data("id");
            var req_type = $(this).data("type");
            var desc = $(this).data("desc");

            $("#id").val(id);
            $("#req-type").val(req_type).attr("disabled", false);
            $("#desc").val(desc).attr("disabled", false);


            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#requestModalTitle").text("Edit Job Post");            
            $("#requestModal").modal("show");
        });

        $(document).on("click", "#btn-view", function(){

            var id = $(this).data("id");
            var title = $(this).data("title");
            var educ = $(this).data("educ");
            var exp = $(this).data("exp");
            var salary = $(this).data("salary");
            var vacancy = $(this).data("vacancy");
            var jcat = $(this).data("jcat");
            var desc = $(this).data("desc");

            $("#title").val(title).attr("disabled", true);
            $("#educ_attain").val(educ).attr("disabled", true);
            $("#expertise").val(exp).attr("disabled", true);
            $("#salary").val(salary).attr("disabled", true);
            $("#vacancy").val(vacancy).attr("disabled", true);
            $("#job_cat").val(jcat).attr("disabled", true);
            $("#description").val(desc).attr("disabled", true);

            $("#btnSave").hide();
            $("#requestModalTitle").text("View Job Post");            
            $("#requestModal").modal("show");

        });


        $(document).on("click", "#btn-delete", function(){
            alert("Calling delete function")
        });


        $("#btnSave").click(function(){

            var cmdType = $(this).text();

            var id = $("#id").val();

            var req_name_key = "#req-name";
            var req_name = $(req_name_key).val();            

            var values = [req_name];
            var keys = [req_name_key];
            

            if (validateItems(values, keys)) {

                companyRequirements(values, company_id);

            } else {
                alert("please fill up the empty items");
            }

        });



        function companyRequirements(params, id) {

            var fd = new FormData();

            fd.append("companyId", id);
            fd.append("req_name", params[0]);
            fd.append("request", "requirement");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    alert("response: " + res.result)
                    console.log(res);
                    location.reload();
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function loadData() {

            var fd = new FormData();
            fd.append("companyId", company_id);
            fd.append("request", "fetch_requirement");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res.result);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl;

            for (var i = 0; i < datas.length; i++) {

                var id = datas[i]["comp_req_id"];
                var req_name = datas[i]["comp_req_desc"];

                tmpl += "<tr>"+
                        "<td>"+ req_name +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ id +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button>"+
                                "</div>"+
                            "</td>"+
                        "</tr>";
            }

            $("#tbl-requirements").find("tbody tr").remove().end();
            $("#tbl-requirements").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }


    });
</script>