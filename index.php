<?php include 'headers/login-header.php'; ?>


        <!-- <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> -->
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="logo">
            <h1 class="h1 header font-weight-normal">D'FAMOUS MANPOWER AND GENERAL SERVICE INC.</h1>
          </div>

        </div>
        <div class="col-md-4">
          
          <div class="form-signin border-green">

            <h1 class="h3 mb-4 font-weight-normal">Company</h1>

            <div class="form-group">
              <label for="inputEmail" class="sr-only">Email address</label>
              <input type="text" id="user" class="form-control" placeholder="Username" required="" autofocus>
            </div>

            <div class="form-group">
              <label for="inputPassword" class="sr-only">Password</label>  
              <input type="password" id="pass" class="form-control" placeholder="Password" required="">
            </div>


            <button id="btn-signin" class="btn btn-lg btn-success btn-block">Sign in</button>
            <button id="btn-forgot-pass" class="btn btn-sm btn-info btn-block">Forgot Password</button>

            <p class="mt-5 mb-3 text-muted">© 2018-2019</p>

          </div>

        </div>        
      </div>      
    </div>


<!-- Modal -->
<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="forgotModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="forgotModalTitle">Forgot Password</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">

                <h6>Please enter email your email address</h6>
                <div class="row">
                        <div class="col-md-12">
                            <label>Email Address</label>
                            <div class="form-group">
                                <input id="email" type="email" class="form-control" placeholder="Email" required/>
                            </div>
                        </div>
                </div>

            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button id="btn-reset-pass" class="btn btn-success">Reset Password</button>
      </div>
    </div>
  </div>
</div>

<?php include 'headers/login-footer.php'; ?>


<script>
  $(document).ready(function(){

      $("#btn-signin").click(function(){
        var user_key = "#user";
        var pass_key = "#pass";

        var user = $(user_key).val();
        var pass = $(pass_key).val();

        var values = [user, pass];
        var keys = [user_key, pass_key];

        if (validateItems(values, keys)) {

            auth(values);

        } else {
            alert("Please input all the empty fields");
        }

      });

      $("#btn-forgot-pass").click(function(){
        $("#forgotModal").modal("show");
      });

      $("#btn-reset-pass").click(function(){
        alert("reset pass")
      })

      function auth(params) {

        var fd = new FormData();
        fd.append("user", params[0]);
        fd.append("pass", params[1]);
        fd.append("request", "login");

        $.ajax({
            type: "POST",
            url: "classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

              if (res.success == true) {

                if (typeof(Storage) !== "undefined") {

                  localStorage.setItem("company_id", res.result[0]["company_id"] );
                  localStorage.setItem("company_name", res.result[0]["company_name"] );
                  localStorage.setItem("company_address", res.result[0]["company_address"] );
                  localStorage.setItem("company_location", res.result[0]["company_location"] );
                  localStorage.setItem("company_desc", res.result[0]["company_desc"] );
                  localStorage.setItem("company_email", res.result[0]["company_email"] );
                  localStorage.setItem("company_contact", res.result[0]["company_contact"] );

                  location.href = "pages/profile.php";

                } else {

                  alert("Your Browser is not supported to our function, please download better browser")

                }

              } else {
                alert(res.result);
              }


            }, error: function() {
                alert("error handler")
            }
        });


      }


      function validateItems(values, keys) {

          var isNotEmpty = false;

          for (var i = 0; i < values.length; i++) {

              if (values[i] == "" || values[i] == null) {
                  $(keys[i]).addClass("border-danger");
                  isNotEmpty = false;
              } else {
                  isNotEmpty = true;
              }
          }

          return isNotEmpty;

      }


  });

</script>