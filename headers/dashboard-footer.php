
        </div>
    </div>

</body>
</html>

<script>
    $(document).ready(function() {

        checkSessions();

        $("#log-out").click(function() {

            localStorage.removeItem("company_id");
            localStorage.removeItem("company_name");
            localStorage.removeItem("company_address");
            localStorage.removeItem("company_location");
            localStorage.removeItem("company_desc");
            localStorage.removeItem("company_email");
            localStorage.removeItem("company_contact");
            // window.localStorage.clear();
            location.href = "../";
        });

        function checkSessions() {
            if (typeof(Storage) !== "undefined") {                
                if (localStorage.length < 0 || localStorage.length == 0) {
                    location.href = "../";
                } else {

                    if (localStorage.getItem("company_name") == null || localStorage.getItem("company_name") == "") {
                        location.href = "../";
                    } else {
                        var name = "Welcome " + localStorage.getItem("company_name") + ", Sign Out";
                        $("#log-out").text(name)

                    }
                }
            } else {

                alert("Your Browser is not supported to our function, please download better browser")
                location.href = "../";

            }
        }

    });


</script>