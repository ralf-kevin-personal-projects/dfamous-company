<!-- <?php include '../config.php'; ?> -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dfamous - Company</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/style.css" />

    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.min.js"></script>    
    <script src="main.js"></script>
</head>

<body>